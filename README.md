# Startup Funding Calculator

Under the /growth.tlb.org directory, a nomograph to calculate how much funding your startup will need to reach profitability.

To test locally, ```$ make dev.growth.tlb.org```. It will start Node.JS and create a server on 127.0.0.1:8000.


